﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float brakepower;
    [SerializeField] private Transform cam;
    private Rigidbody rigidBody;
    private Vector3 startPosition;
    private float horizontalVelocity;
    private float verticalVelocity;
    private float timer;
    private bool brake;
    private bool inSpeedPlataform;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.maxAngularVelocity = 70;
        startPosition = rigidBody.position;
    }

    // Update is called once per frame
    void Update()
    {
        horizontalVelocity = speed * Input.GetAxis("Horizontal");
        verticalVelocity = speed * Input.GetAxis("Vertical");
        brake = Input.GetButton("Brake");

        if (Input.GetKeyDown(KeyCode.R) && Time.timeScale > 0)
        {
            rigidBody.position = startPosition;
            rigidBody.velocity = new Vector3(0,0,0);
        }
    }

    private void FixedUpdate()
    {
        timer += Time.deltaTime;

        if (brake)
        {
            rigidBody.velocity = new Vector3(rigidBody.velocity.x / brakepower, rigidBody.velocity.y, rigidBody.velocity.z / brakepower);
        }
        else
        {
            Vector3 forward = cam.forward;
            Vector3 up = cam.up;
            Vector3 right = cam.right;

            rigidBody.AddForce(new Vector3(verticalVelocity * (forward.x + up.x), 0, verticalVelocity * (forward.z + up.z)));
            rigidBody.AddForce(new Vector3(horizontalVelocity * right.x, 0, horizontalVelocity * right.z));
        }

        if (inSpeedPlataform && timer >= 1)
        {
            Vector3 normalizedVelocity = rigidBody.velocity.normalized;
            rigidBody.velocity = new Vector3(normalizedVelocity.x * 10 + rigidBody.velocity.x,
                rigidBody.velocity.y,
                normalizedVelocity.z * 10 + rigidBody.velocity.z);
            timer = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, 10, rigidBody.velocity.z);
        }

        if (other.gameObject.layer == 9)
        {
            inSpeedPlataform = true;

            Vector3 normalizedVelocity = rigidBody.velocity.normalized;
            rigidBody.velocity = new Vector3(normalizedVelocity.x * 10 + rigidBody.velocity.x,
                rigidBody.velocity.y,
                normalizedVelocity.z * 10 + rigidBody.velocity.z);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        inSpeedPlataform = false;
    }
}
