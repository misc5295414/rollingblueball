﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderValue : MonoBehaviour
{
    void Start()
    {
        float volume = PlayerPrefs.GetFloat("Volume");
        print(volume);
        GetComponent<Slider>().value = volume;
    }
}
