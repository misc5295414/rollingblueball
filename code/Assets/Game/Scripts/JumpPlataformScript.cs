﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPlataformScript : MonoBehaviour
{
    private Material plataformMaterial;
    private Color emissiveColor;
    private float timer;
    private float fadingAmount;

    // Start is called before the first frame update
    void Start()
    {
        plataformMaterial = GetComponent<Renderer>().material;
        emissiveColor = new Color(0.04f, 0.59f, 0.785f);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > 0.01f)
        {
            if (emissiveColor.g >= 0.59f)
            {
                fadingAmount = -0.01f;
            }
            else if (emissiveColor.g <= 0)
            {
                fadingAmount = 0.01f;
            }

            emissiveColor.g += fadingAmount;
            plataformMaterial.SetColor("_EmissionColor", emissiveColor);
            timer = 0;
        }
    }
}
